

CREATE DATABASE course_db;

DROP DATABASE course_db;

CREATE DATABASE course_db;

SHOW DATABASES;

USE course_db;

CREATE TABLE students(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE subjects(
	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(100) NOT NULL,
	schedule VARCHAR(100) NOT NULL,
	instructor VARCHAR(100) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE enrollments(
	id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	subject_id INT NOT NULL,
	datetime_created TIMESTAMP NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_enrollments_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_enrollments_subject_id
		FOREIGN KEY (subject_id) REFERENCES subjects(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
);


INSERT INTO students (	username, password, full_name, email ) 
VALUES 
("lulusupport", "lulu", "Lulu Yordle", "lulu@school.com"),
("johndoe", "john123", "John Doe", "john@mail.com"),
("janedoe", "jane123", "John Doe", "jane@mail.com"),
("paul", "paul", "Jame Paul Lincuna", "paul@mail.com");


INSERT INTO subjects (	course_name, schedule, instructor) 
VALUES 
("Sorcery", "05:00 PM - 10:00 PM", "Ms. Camille"),
("MySQL", "05:00 PM - 10:00 PM", "Ms. Camille"),
("MCP", "08:00 AM - 05:00 PM", "Ms. Riza"),
("Java", "08:00 AM - 05:00 PM", "Mr. Rupert");


INSERT INTO enrollments (student_id, subject_id, datetime_created) 
VALUES 
(1, 1, "2023-05-03 20:42:50"),
(2, 2, "2023-05-03 20:42:50"),
(3, 3, "2023-05-03 20:42:50"),
(4, 4, "2023-05-03 20:42:50");
