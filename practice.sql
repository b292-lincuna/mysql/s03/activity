
USE music_db;

DROP TABLE reviews;

CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO reviews (	review, datetime_created, rating ) 
VALUES 
("The songs are okay. Worth the subscription", "2023-05-03 00:00:00", 5),
("The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1),
("Add Bruno Mars and Lady Gaga", "2023-03-23 02:00:00", 4),
("I want to listen to more k-pop", "2022-09-23 04:00:00", 3),
("Kindly add more OPM", "2023-02-01 05:00:00", 5);

SELECT * FROM reviews;
SELECT * FROM reviews WHERE rating =5;
SELECT * FROM reviews WHERE rating =1;

UPDATE reviews SET rating=5;